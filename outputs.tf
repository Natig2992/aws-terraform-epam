
output "vpc_id" {
  value = aws_vpc.aws_vpc_epam.id

}

output "availability_zones" {
   value =  data.aws_availability_zones.available.names

}

output "aws_lb_public_dns" {

    value = "http://${aws_lb.web-epam-wordpress-alb.dns_name}"

}

output "aws_efs_dns_name" {

value = "EFS dns name: ${aws_efs_file_system.epam_myefs_wordpress.dns_name}"

}


output "aws_rds_endpoint" {

 value = "DB_endpoint: ${aws_db_instance.mysql_epam.endpoint}"

}

output "aws_instances_public_dns" {

 value = [aws_instance.wordpress-servers[0].public_dns, aws_instance.wordpress-servers[1].public_dns]

} 


