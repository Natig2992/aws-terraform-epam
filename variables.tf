variable "instance_type" {
  default = "t2.micro"
}

variable "num_servers" {
  default = 1
}

variable "my_ami" {
  default = "ami-0d527b8c289b4af7f"
}

variable "my_cidr_vpc" {
  default = "10.0.0.0/20"
}

variable "my_cidr_subnets" {
  description = "My subnets for two subnets in diff. availability zones"
  type        = list(string)
  default     = ["10.0.8.0/24", "10.0.9.0/24"]

}

variable "my_public_subnets" {
  description = "My two public subnets"
  type        = list(string)
  default     = ["10.0.10.0/24", "10.0.11.0/24"]
}

variable "environment" {
  default = "dev"
}

variable "app_name" {
  default = "hello"
}

variable "ssh_key" {
  default = "test"
}

variable "rds_credentials" {
  type = object({
    username = string
    password = string
    dbname   = string
  })

  default = {
    username = "${wordpress_user}"
    password = "${wordpress_password}"
    dbname   = "${wordpress_db}"
  }

  description = "Master DB username, password and dbname for RDS"
}

variable "counts" {
  default = 2
}

variable "userdatas" {
   default = ["./wordpress.tpl", "./wordpress2.tpl"]
}

