#!/bin/bash

# Vars
db_username=${db_username} 
db_password=${db_password} 
db_name=${db_name} 
db_RDS=${db_RDS} 
efs_dns=${efs_dns} 
url_alb=${url_alb}

mount_efs(){
sudo mount -t efs $efs_dns:/ /var/www/html
	#edit fstab
sudo cat <<EOF >>/etc/fstab
$efs_dns:/   /var/www/html   efs   defaults,_netdev  0  0
EOF
}

install_php(){
	# Install php7.4 and imagick
sudo amazon-linux-extras enable php7.4
sudo yum clean metadata
sudo yum install -y php php-{pear,cgi,common,curl,mbstring,gd,mysqlnd,gettext,bcmath,json,xml,fpm,intl,zip,imap,devel} gcc ImageMagick ImageMagick-devel ImageMagick-perl

yes '' | pecl install imagick
sudo chmod 755 /usr/lib64/php/modules/imagick.so
sudo cat <<EOF >>/etc/php.d/20-imagick.ini
extension=imagick
EOF

sudo systemctl restart php-fpm.service
}

install_packages(){
	# Install Apache and efs-utils
sudo yum update -y
sudo yum install -y httpd amazon-efs-utils
}

check_installed_wordpress(){
	if [ ! -f /var/www/html/wp-config.php ];
	then
		install_wordpress
	fi
}

install_wordpress(){
	install_php
	# Download latest version
sudo find /var/www -type d -exec chmod 2775 {} \;
sudo find /var/www -type f -exec chmod 0664 {} \;
sudo curl https://wordpress.org/latest.tar.gz | tar zx --strip-components=1 -C /var/www/html/
sudo chown apache:apache /var/www/html -R

	# Create config
	cd /var/www/html
sudo cp wp-config-sample.php wp-config.php
sudo sed -i "s/localhost/${db_RDS}/g" wp-config.php
sudo sed -i "s/database_name_here/${db_name}/g" wp-config.php
sudo sed -i "s/username_here/${db_username}/g" wp-config.php
sudo sed -i "s/password_here/${db_password}/g" wp-config.php
sudo cat <<EOF >>/var/www/html/wp-config.php
define( 'FS_METHOD', 'direct' );
define('WP_MEMORY_LIMIT', '128M');
EOF

	# Install via cli
wget https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar -O /usr/local/sbin/wp
sudo chmod +x /usr/local/sbin/wp
sudo -u apache /usr/local/sbin/wp core install --url=http://${url_alb} --title='Nagiev Natig  AWS task by EPAM' --admin_user='admin' --admin_password='${db_password}' --admin_email='admin@local.aws'
	# Install custom theme
sudo -u apache /usr/local/sbin/wp theme install twentyseventeen --activate
	# Change header to EC2-Hostname
#sudo sed '11 a echo '<div align="center"><b>My hostname by AWS</b></div>'; echo str_repeat('&nbsp;', 118); echo gethostname();' /var/www/html/wp-content/themes/twentytwentytwo/functions.php

#####Other vars################
sudo sed -i "s|bloginfo( 'name' )|echo shell_exec('hostname')|g" /var/www/html/wp-content/themes/twentyseventeen/template-parts/header/site-branding.php

#sudo sed -i "s|bloginfo( 'name' )|echo shell_exec('hostname')|g" /var/www/html/wp-content/themes/twentynineteen/template-parts/header/site-branding.php
}
##############################
configure_apache(){
	# Enable .htaccess
sudo sed -i '/<Directory "\/var\/www\/html">/,/<\/Directory>/ s/AllowOverride None/AllowOverride all/' /etc/httpd/conf/httpd.conf

	# Enable and restart service
sudo systemctl enable  httpd.service
sudo systemctl restart httpd.service
}


start(){
	install_packages
	mount_efs
	check_installed_wordpress
	configure_apache
}

start
