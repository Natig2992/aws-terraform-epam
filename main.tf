provider "aws" {

  region = "eu-central-1"
}

terraform {
  backend "s3" {
    bucket = "my-terraform-udemy"    // Bucket where to SAVE Terraform State
    key    = "dev/terraform.tfstate" // Object name in the bucket to SAVE Terraform State
    region = "eu-central-1"          // Region where bucket created
  }
}

#----------------Data--------------------------------#


data "aws_availability_zones" "available" {}


data "template_file" "user_data_wordpress" {
  template    = file("./user_data.tpl")
  vars = {
    db_username = var.rds_credentials.username
    db_password = var.rds_credentials.password
    db_name     = var.rds_credentials.dbname
    db_RDS      = aws_db_instance.mysql_epam.endpoint
    efs_dns     = aws_efs_file_system.epam_myefs_wordpress.dns_name
    url_alb     = aws_lb.web-epam-wordpress-alb.dns_name
  } 
  depends_on  = [aws_db_instance.mysql_epam, aws_efs_file_system.epam_myefs_wordpress]
  
}


############Other user data templates#################################
/*
data "template_file" "user_data" {
  template = "${file("${element(var.userdatas, count.index)}")}"
  # template = file("./wordpress.tpl") 
  count    = "${length(var.userdatas)}"
  vars = {
    db_username      = var.rds_credentials.username
    db_user_password = var.rds_credentials.password
    db_name          = var.rds_credentials.dbname
    db_RDS           = aws_db_instance.mysql_epam.endpoint
    efs_dns          = aws_efs_file_system.epam_myefs_wordpress.dns_name
  }
  depends_on = [aws_db_instance.mysql_epam, aws_efs_file_system.epam_myefs_wordpress, aws_instance.for_ami]
}

data "template_file" "user_data2" {
  template = file("./wordpress2.tpl")
  vars = {
    efs_dns = aws_efs_file_system.epam_myefs_wordpress.dns_name
  
  }
  depends_on = [aws_db_instance.mysql_epam, aws_efs_file_system.epam_myefs_wordpress, aws_instance.for_ami]
}

*/
###################################################################

#------------------------------------------------------------

/*
resource "aws_instance" "for_ami" {
  ami                    = "ami-0eb7496c2e0403237"
  instance_type          = "t2.micro"
  subnet_id              = aws_subnet.aws_subnet_public_epam[0].id
  vpc_security_group_ids = [aws_security_group.my_webserver_epam.id]
  key_name               = "aws-epam-1"
  tags = {
    Name  = "Test_instance"
    Owner = "Nagiev Natig"
  }
  depends_on = [aws_efs_file_system.epam_myefs_wordpress, aws_efs_mount_target.epam_myefs_mount_wordpress]
}


resource "aws_ami_from_instance" "apache_php_wordpress" {
  name               = "apache_php_wordpress"
  source_instance_id = aws_instance.for_ami.id
  depends_on = [aws_instance.for_ami]

  timeouts {
    create = "10m"
  }
}

*/


resource "aws_instance" "wordpress-servers" {
  count                  = 2
  ami                    = "ami-0eb7496c2e0403237"
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.my_webserver_epam.id]
  subnet_id              = aws_subnet.aws_subnet_private_epam[count.index].id
  key_name               = "aws-epam-1"
  user_data = data.template_file.user_data_wordpress.rendered
  tags = {
    Name  = "Server wordpress-${element(data.aws_availability_zones.available.names, count.index)}"
    Owner = "Nagiev Natig"
  }
  depends_on = [aws_vpc.aws_vpc_epam, aws_efs_file_system.epam_myefs_wordpress, aws_db_instance.mysql_epam]
}



resource "aws_efs_file_system" "epam_myefs_wordpress" {
  encrypted = true
  tags = {
    Name = "My efs for EPAM in ${var.environment}"
  }

}

resource "aws_efs_mount_target" "epam_myefs_mount_wordpress" {
  count           = 2
  file_system_id  = aws_efs_file_system.epam_myefs_wordpress.id
  subnet_id       = aws_subnet.aws_subnet_private_epam[count.index].id
  security_groups = [aws_security_group.my_efs_epam_sg.id]
  depends_on      = [aws_efs_file_system.epam_myefs_wordpress, aws_security_group.my_efs_epam_sg]
}

resource "aws_db_instance" "mysql_epam" {
  identifier                      = "mysql-epam"
  engine                          = "mysql"
  engine_version                  = "5.7.33"
  instance_class                  = "db.t2.micro"
  db_subnet_group_name            = aws_db_subnet_group.epam_db_sg.name
  enabled_cloudwatch_logs_exports = ["general", "error"]
  db_name                         = var.rds_credentials.dbname
  username                        = var.rds_credentials.username
  password                        = var.rds_credentials.password
  allocated_storage               = 20
  max_allocated_storage           = 0
  backup_retention_period         = 7
  backup_window                   = "00:00-00:30"
  maintenance_window              = "Sun:17:30-Sun:18:30"
  storage_type                    = "gp2"
  vpc_security_group_ids          = [aws_security_group.my_rds_epam_sg.id]
  skip_final_snapshot             = true
  depends_on                      = [aws_security_group.my_rds_epam_sg, aws_db_subnet_group.epam_db_sg]
  multi_az                        = true
}






#----------Secruity Groups---------------------------------#

resource "aws_security_group" "my_webserver_epam" {

  name   = "Dynamic Security Group for EPAM"
  vpc_id = aws_vpc.aws_vpc_epam.id
  dynamic "ingress" {

    for_each = ["80", "443", "22"]

    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = [aws_vpc.aws_vpc_epam.cidr_block]
      # cidr_blocks = ["0.0.0.0/0"]
    }

  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "Dynamic sec group for EPAM"
    Owner = "Nagiev Natig"

  }
}

resource "aws_security_group" "my_alb_epam" {
  name   = "Dynamic Secuirty Group fo ALB EPAM"
  vpc_id = aws_vpc.aws_vpc_epam.id

  dynamic "ingress" {

    for_each = ["80", "443"]

    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]

    }

  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "Dynamic sec group for alb EPAM"
    Owner = "Nagiev Natig"

  }

}


resource "aws_security_group" "my_efs_epam_sg" {
  name        = "my_efs_epam_sg"
  description = "Allow NFS inbound traffic"
  vpc_id      = aws_vpc.aws_vpc_epam.id

  ingress {
    description     = "NFS from EC2"
    from_port       = 2049
    to_port         = 2049
    protocol        = "tcp"
    security_groups = [aws_security_group.my_webserver_epam.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  depends_on = [aws_security_group.my_webserver_epam]

  tags = {
    Name  = "Security group for efs"
    Owner = "Nagiev Natig"
  }

}


resource "aws_security_group" "my_rds_epam_sg" {
  name        = "sg_for_my_epam_proj"
  description = "Allow MySQL inbound traffic"
  vpc_id      = aws_vpc.aws_vpc_epam.id

  ingress {
    description     = "RDS from EC2"
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    security_groups = [aws_security_group.my_webserver_epam.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  depends_on = [aws_security_group.my_webserver_epam]

  tags = {
    Name  = "Security group for rds"
    Owner = "Nagiev Natig"
  }
}


#----------------------------------------------------------------------


#Load Balancer
/*
resource "aws_elb" "web-wordpress" {
  name               = "WebServer-HA-ELB"
  internal           = false
  load_balancer_type = "application"
  availability_zones = [data.aws_availability_zones.available.names[0], data.aws_availability_zones.available.names[1]]
  security_groups    = [aws_security_group.my_elb_epam.id]
  instances = [aws_instance.wordpress-servers[0].id, aws_instance.wordpress-servers[1].id] 
  

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:80/"
    interval            = 10
  }

  tags = {
    Name = "WebServer-Highly-Available-ELB"
  }
}
*/
#-------------------------------------------------------------------------

resource "aws_lb" "web-epam-wordpress-alb" {

  name               = "web-epam-alb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.my_alb_epam.id]
  subnets            = [aws_subnet.aws_subnet_public_epam[0].id, aws_subnet.aws_subnet_public_epam[1].id]
}

resource "aws_lb_listener" "front_end" {
  load_balancer_arn = aws_lb.web-epam-wordpress-alb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_lb_target_group.wordpress-epam-tg.id
    type             = "forward"
  }

  depends_on = [
    aws_lb_target_group.wordpress-epam-tg
  ]
}

resource "aws_lb_target_group" "wordpress-epam-tg" {
  port                          = 80
  protocol                      = "HTTP"
  vpc_id                        = aws_vpc.aws_vpc_epam.id
  load_balancing_algorithm_type = "round_robin"
  target_type                   = "instance"
  health_check {
    path                = "/"
    healthy_threshold   = 2
    interval            = 30
    protocol            = "HTTP"
    unhealthy_threshold = 2
  }

  depends_on = [
    aws_lb.web-epam-wordpress-alb
  ]

  lifecycle {
    create_before_destroy = true
  }
}


resource "aws_lb_target_group_attachment" "wordpress-epam-tg-atch" {
  count            = 2
  target_group_arn = aws_lb_target_group.wordpress-epam-tg.arn
  target_id        = aws_instance.wordpress-servers[count.index].id
  port             = 80
}


