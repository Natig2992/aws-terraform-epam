#-----------------------------------------------------

#data "aws_availability_zones" "available" {}


resource "aws_vpc" "aws_vpc_epam" {
  cidr_block                       = var.my_cidr_vpc
  instance_tenancy                 = "default"
  enable_dns_support               = "true"
  enable_dns_hostnames             = "true"
  assign_generated_ipv6_cidr_block = "false"
  tags = {
    Name  = "my-vpc-epam"
    Owner = "Natig"
  }
}


resource "aws_subnet" "aws_subnet_public_epam" {
  count                   = 2
  vpc_id                  = aws_vpc.aws_vpc_epam.id
  cidr_block              = element(var.my_public_subnets, count.index)
  availability_zone       = element(data.aws_availability_zones.available.names, count.index)
  map_public_ip_on_launch = true
  tags = {
    Name        = "Public subnet in ${var.environment}"
    Environment = "${var.environment}"
  }
}



resource "aws_subnet" "aws_subnet_private_epam" {
  count                   = 2
  cidr_block              = element(var.my_cidr_subnets, count.index)
  vpc_id                  = aws_vpc.aws_vpc_epam.id
  map_public_ip_on_launch = "false"
  availability_zone       = element(data.aws_availability_zones.available.names, count.index)
  tags = {
    Name        = "Private subnet-${count.index + 1}-in-${var.environment}"
    Environment = "${var.environment}"
  }
}




/*
resource "aws_subnet" "aws_private_db_subnet" {

  cidr_block = var.my_cidr_subnets[2]
  vpc_id    = aws_vpc.aws_vpc_epam.id
  availability_zone = data.aws_availability_zones.available.names[2] 
}
*/

#Internet Gateway

resource "aws_internet_gateway" "my_epam_gw" {
  vpc_id = aws_vpc.aws_vpc_epam.id
  tags = {
    Name = "My_igw-for-vpc-EPAM"
  }
}
#--------Route Table for Public subnet------------------

resource "aws_route_table" "my_epam_rt" {
  vpc_id = aws_vpc.aws_vpc_epam.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.my_epam_gw.id
  }



  tags = {
    Name = "Route table for ${aws_vpc.aws_vpc_epam.id}"
  }
}
#------------------Route Table for Private subnet--------------------

resource "aws_route_table" "my_epam_private_rt" {
  count = 2
  vpc_id = aws_vpc.aws_vpc_epam.id

  route {
    cidr_block = "0.0.0.0/0"
     nat_gateway_id = aws_nat_gateway.nat_epam_wordpress[count.index].id
  }



  tags = {
    Name = "Route table for private subnet ${aws_vpc.aws_vpc_epam.id}"
  }
}
#------------------Route Table association  for Public subnet--------------------

resource "aws_route_table_association" "my_epam_rt_assoc" {
  count          = 2
  subnet_id      = aws_subnet.aws_subnet_public_epam[count.index].id
  route_table_id = aws_route_table.my_epam_rt.id

}


#------------------Route Table association for Private subnet--------------------

resource "aws_route_table_association" "my_epam_rt_assoc_private" {
  count          = 2
  subnet_id      = aws_subnet.aws_subnet_private_epam[count.index].id
  route_table_id = aws_route_table.my_epam_private_rt[count.index].id

}

#-----------------------------------------------------------------------------------

resource "aws_eip" "nat_eip_epam" {
  count = 2
  vpc        = true
  depends_on = [aws_internet_gateway.my_epam_gw]
}

resource "aws_nat_gateway" "nat_epam_wordpress" {
  count = 2
  allocation_id = aws_eip.nat_eip_epam[count.index].id
  subnet_id     = aws_subnet.aws_subnet_public_epam[count.index].id
  depends_on    = [aws_internet_gateway.my_epam_gw]
  tags = {
    Name        = "nat-gateway-epam-for-${aws_subnet.aws_subnet_private_epam[count.index].id}"
    Environment = "${var.environment}"
  }
}

resource "aws_db_subnet_group" "epam_db_sg" {
  name       = "epam_db_subnet_group"
  subnet_ids = [aws_subnet.aws_subnet_private_epam[0].id, aws_subnet.aws_subnet_private_epam[1].id]
}
